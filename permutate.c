/**
 * Copyright (C) 2016 Sylvester Rac
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, 
 * USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <stdbool.h>

#define PRINTERVAL 20 /** Print Progress Interval (seconds) */
#define VERSION_MAJOR 1
#define VERSION_MINOR 0

// make_permutations_1 - replace one character at a time with all possible values
// make_permutations_2 - replace two characters at a time with all possible values
// make_permutations_3 - insert a character at each position of s and replace with all possible values
// make_permutations_4 - insert a character at each position of s and replace with all possible values, also perform substitution of one existing character
// make_permutations_5 - insert two characters at each possible position of s and replace with all possible values

char normalchars1[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
char specialchars1[] = "~!@#$%^&*()+-_={}\"[]\\|':;,./<>`?";
bool test = false;
int testCount = 0;
//char normalchars1[] = "abcd";
//char specialchars1[] = "123";

/** print some help for the user */
static void
printHelp(char *progname) {
  printf("Password permutation generator\n"
	 "Generates permutations for each line read from stdin\n"
	 "see permutations.txt for more information\n"
	 "\n"
	 "Usage: %s [OPTIONS]\n"
	 "\n"
	 "OPTIONS:\n"
	 "-h, --help\t\tShow this help screen\n"
	 "-s, --stdchars\t\tOnly use 0-9, upper and lower case alphabetical\n"
	 "\t\t\tcharacters for permutations\n"
	 "-t, --test\t\tTest and report on number of permutations generated\n"
	 "-v, --version\t\tPrint version and exit\n",
	 progname);
}


// output a string, or count if test is set
void outputString(char *s) {
	if (!test) {
		printf("%s\n", s);
	}
	else {
		testCount++;
	}
}


// swap 2 characters of string s
void swapChars(char *s, int pos1, int pos2) {
	char tmp = s[pos2];
	s[pos2] = s[pos1];
	s[pos1] = tmp;
}


// substitute single character from string s, at position pos, with all values in subst
void substituteSingleChar(char *s, int pos, char *subst, int subst_len)
{
		char original_char = s[pos];
		// substitute every character from subst into position pos
		int j;
		for (j=0; j<subst_len; j++) {
			s[pos] = subst[j];
			outputString(s);
		}
		s[pos] = original_char;
}


// substitute two characters from string s, at position pos1 and pos2, with combinations of all values in subst
void substituteTwoChars(char *s, int pos1, int pos2, char *subst, int subst_len)
{
	char original_char1 = s[pos1];
	char original_char2 = s[pos2];
	// substitute every 2 character combination from subst into position pos1, pos2
	int i, j;
	for (i=0; i<subst_len; i++) {
		s[pos1] = subst[i];
		for (j=0; j<subst_len; j++) {
			s[pos2] = subst[j];
			outputString(s);
		}
	}
	s[pos1] = original_char1;
	s[pos2] = original_char2;
}


// replace one character at a time with all possible values
void make_permutations_1(char *s, char *subst)
{
	//
	//char allchars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~!@#$%^&*()-_=+,./<>?;':\"\\|[]{}";
	//
	int i;
	int n_len = strlen(subst);
	outputString(s);
	int len = strlen(s);
	for (i=0; i<len; i++) {
		substituteSingleChar(s, i, subst, n_len);
	}
}


// replace two characters at a time with all possible values
void make_permutations_2(char *s, char *subst)
{
	int i, j;
	int n_len = strlen(subst);
	outputString(s);
	int len = strlen(s);
	// traverse the string, selecting all possible combinations of characters to substitute
	for (i=0; i<len-1; i++) {
		for (j=i+1; j<len; j++) {
			// we substitute every combination of characters for position i, j
			substituteTwoChars(s, i, j, subst, n_len);
		}
	}
}


// insert a character at each position of s and replace with all possible values
void make_permutations_3(char *s, char *subst)
{
	// allocate space for strlen(s) + 1 on the heap
	int s_len = strlen(s);
	char *new_s = malloc(s_len + 2);
	new_s[0] = '.';
	// copy existing string over
	int i;
	for (i=0; i<s_len; i++) new_s[i+1] = s[i];
	new_s[s_len + 1] = 0;
	int n_len = strlen(subst);
	// change position (swap) characters, using all possible values each time
	for (i=0; i<s_len; i++) {
		substituteSingleChar(new_s, i, subst, n_len);
		//printf("%s\n", new_s);
		// swap new_s[i] with new_s[i+1]
		swapChars(new_s, i, i+1);
	}
	substituteSingleChar(new_s, s_len, subst, n_len);
	//printf("%s\n", new_s);
	free(new_s);
}


// insert a character at each position of s and replace with all possible values, 
// but also perform substitution of one existing character
void make_permutations_4(char *s, char *subst)
{
	// allocate space for strlen(s) + 1 on the heap
	int s_len = strlen(s);
	char *new_s = malloc(s_len + 2);
	new_s[0] = '.';
	// copy existing string over
	int i;
	for (i=0; i<s_len; i++) new_s[i+1] = s[i];
	new_s[s_len + 1] = 0;
	int n_len = strlen(subst);
	// change position (swap) characters, using all possible values each time
	int j;
	for (i=0; i<s_len; i++) {
		for (j=i+1; j<s_len+1; j++) {
			// we substitute every combination of characters for position i, j
			substituteTwoChars(new_s, i, j, subst, n_len);
		}
		//printf("%s\n", new_s);
		// swap new_s[i] with new_s[i+1]
		char x = new_s[i+1];
		new_s[i+1] = new_s[i];
		new_s[i] = x;
	}
	//printf("%s\n", new_s);
	free(new_s);
}


// insert two characters at each possible position of s and replace with all possible values
void make_permutations_5(char *s, char *subst)
{
	// allocate space for strlen(s) + 2 on the heap
	int s_len = strlen(s);
	char *new_s = malloc(s_len + 3);
	new_s[0] = '.';
	new_s[1] = '.';
	// copy existing string over
	int i;
	for (i=0; i<s_len; i++) new_s[i+2] = s[i];
	s_len += 2;
	new_s[s_len] = 0;
	int n_len = strlen(subst);
	// change position (swap) characters, using all possible values each time
	int j, k;
	for (i=0; i<s_len-1; i++) {
		for (j=i+1; j<s_len; j++) {
			// we substitute every combination of characters for position i, j
			substituteTwoChars(new_s, i, j, subst, n_len);
			//printf("%s %d %d\n", new_s, i, j);
			if (j == s_len - 1) {
				// return 2nd character to it's original position, 
				// then move 1st and 2nd over by one
				for (k=j; k>i; k--) swapChars(new_s, k, k-1);
				swapChars(new_s, i+1, i+2);
				swapChars(new_s, i, i+1);
				//printf("%s\n", new_s);
			}
			else {
				swapChars(new_s, j, j+1);
				//printf("%s\n", new_s);
			}
		}
	}
	free(new_s);
}


int main(int argc, char** argv) {

  int ret = 0;
  bool stdchars = false;

  /** Parse arguments */
  while(true) {
    int c, option_index;
    static struct option long_options[] = {
      {"help",     no_argument      , 0, 'h'},
      {"stdchars", no_argument      , 0, 's'},
      {"test",     no_argument      , 0, 't'},
      {"version",  no_argument      , 0, 'v'},
      {0, 0, 0, 0}};
    /* getopt_long stores the option index here. */
    option_index = 0;

    c = getopt_long(argc, argv, "hi:stv",
			long_options, &option_index);

    /* Detect the end of the options. */
    if(c == -1)
      break;

    switch(c) {
    case 'h':
      printHelp(argv[0]);
      return 0;
    case 's':
      stdchars = true;
      break;
    case 't':
      test = true;
      break;
    case 'v':
      printf("password permutator version %d.%d\n", VERSION_MAJOR, VERSION_MINOR);
      return 0;

    default:
      printHelp(argv[0]);
      ret = 1;
    }
  }

  char inputStr[40];
  char *substchars;

  // concatenate normalchars1 and specialchars1, unless stdchars was specified
  if (!stdchars) {
    substchars = malloc(strlen(normalchars1) + strlen(specialchars1) + 1);
    strcpy(substchars, normalchars1);
    strcat(substchars, specialchars1);
  }
  else {
    substchars = normalchars1;
  }

  // read up to 39 characters from stdin, remove newline from output,
  // if no newline was read flush remaining input so it doesn't affect the next input operation
  while (fgets(inputStr, 40, stdin) != NULL) {
    // minimum password length 3 characters, remove the newline character first
    int inputLen = strlen(inputStr);
    if (inputLen > 0 && inputStr[inputLen - 1] != '\n') {
      int ch;
      while ( ((ch = getchar()) != '\n') && (ch != EOF) );
    }
    else {
      inputStr[inputLen - 1] = 0;
    }
    // break out on empty string
    if (strlen(inputStr) == 0)
      break;
    // ignore inputs of three characters or less
    if (strlen(inputStr) > 3) {
      make_permutations_1(inputStr, substchars);
      make_permutations_2(inputStr, substchars);
      make_permutations_3(inputStr, substchars);
      make_permutations_4(inputStr, substchars);
      make_permutations_5(inputStr, substchars);
    }
    else {
      fprintf(stderr, "%s is too short and will be ignored.\n", inputStr);
    }
  }

  if (test) printf("%d\n", testCount);

  free(substchars);

  exit(ret);

}

